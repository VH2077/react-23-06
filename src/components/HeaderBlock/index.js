import React from 'react';
import s from './HeaderBlock.module.scss';

import ReactLogo from './img/logo.png';
import {ReactComponent as ReactLogoSvg} from '../../logo.svg';



const HeaderBlock =({ hideBackground = false, children})=>{
    const styleCover = hideBackground ? {background: 'none'} : {};
    return (
        <div className={s.cover} style = {styleCover}>
            <div className={s.wrap}>
                {children}
            </div>        
        </div>
    )
}

export default HeaderBlock;