import React from 'react';
import h from './Header.module.scss';

const Header = ({children})=>{
    return (
        <>
           <h1 className = {h.header}>{children}</h1> 
        </>
    )
}

export default Header;