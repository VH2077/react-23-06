import React from 'react';
import p from './Paragraph.module.scss'

const Paragraph = ({children})=>{
    return (
        <>
          <p className={p.decr}>{children}</p>
        </>
    )
}
export default Paragraph;