import React from 'react';
import HeaderBlock from './components/HeaderBlock';
import Paragraph from './components/Paragraph';
import Header from './components/Header';

import {ReactComponent as ReactLogoSvg} from './logo.svg';

const App = ()=>{
  return (
    <>
         <HeaderBlock >
          <Header>It is time to learn words</Header>
          <Paragraph>Please, use for lean cards and a head.</Paragraph>
         </HeaderBlock>
         
     <HeaderBlock  hideBackground>
        <Header>It is next title</Header>
         <Paragraph>Please, use for lean cards and a head.</Paragraph>
         <ReactLogoSvg/>
     </HeaderBlock>
     
     </>
  )
}

export default App;
